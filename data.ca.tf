data "terraform_remote_state" "ca" {
  backend = "gcs"

  config = {
    bucket = var.certificate_authority_remote_state_bucket
    prefix = var.certificate_authority_remote_state_prefix
  }
}
