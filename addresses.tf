resource "google_compute_address" "public" {
  count = var.cluster_size

  name   = "${var.cluster_name}-public-${count.index}"
  region = var.region
}
