variable "project" {
  type        = string
  description = "Google Cloud project name"
}

variable "zone" {
  type = string
}

variable "region" {
  type = string
}

variable "cluster_name" {
  type        = string
  description = "Cluster name (multiple Consul clusters can be deployed in the same project if they have different names)"
}

variable "network" {
  type        = string
  description = "Name or self-link of the network in which to deploy this cluster"
}

variable "cluster_size" {
  default = 3
  type    = number

  validation {
    condition     = tonumber(var.cluster_size) == 3 || tonumber(var.cluster_size) == 5
    error_message = "The cluster must contain either 3 or 5 members."
  }
}

variable "allowed_ip_cidrs" {
  type        = list(string)
  default     = []
  description = "A list of the public IP CIDR blocks that are allowed to reach the Consul cluster"
}

variable "primary_datacenter" {
  type        = string
  description = "The name of the primary datacenter for ACLs"
}

variable "datacenter" {
  type        = string
  description = "The name of the datacenter for the cluster"
}

variable "ui_enabled" {
  default     = true
  description = "The fully-qualified domain name to use on TLS server certificates"
}

variable "ca_file" {
  type        = string
  description = "The root certificate authority to trust when connecting to Vault"
}

variable "connect_private_key" {
  type        = string
  description = "Connect root certificate authority private key."
}

variable "connect_root_cert" {
  type        = string
  description = "Connect root certificate authority certificate."
}

variable "network_tags" {
  type        = list(string)
  default     = []
  description = "Optional additional network tags to apply to instances"
}

variable "labels" {
  type        = map(string)
  default     = {}
  description = "Optional additional labels tags to apply to instances"
}

variable "certificate_authority_remote_state_bucket" {
  type = string
}

variable "certificate_authority_remote_state_prefix" {
  type = string
}

variable "machine_type" {
  type = string
}

variable "external_domain" {
  type = string
}

variable "dns_managed_zone" {
  type = string
}
