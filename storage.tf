resource "google_compute_disk" "consul" {
  count = var.cluster_size

  name  = "${var.cluster_name}-data-${count.index}"
  type  = "pd-ssd"
  zone  = var.zone
  size  = 10
}
