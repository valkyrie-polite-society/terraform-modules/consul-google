resource "google_compute_instance" "consul" {
  count = var.cluster_size

  name         = "${var.cluster_name}-${count.index}"
  machine_type = var.machine_type
  zone         = var.zone

  tags = flatten([
    local.auto_join_tag,
    var.network_tags
  ])

  labels = var.labels

  boot_disk {
    initialize_params {
      image = data.google_compute_image.configuration_store.self_link
    }
  }

  attached_disk {
    source      = google_compute_disk.consul[count.index].self_link
    device_name = "consul"
  }

  network_interface {
    network = data.google_compute_network.default.self_link
    access_config {
      nat_ip = google_compute_address.public[count.index].address
    }
  }

  scheduling {
    preemptible       = true
    automatic_restart = false
  }

  service_account {
    scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }

  metadata = {
    "user-data" = templatefile("${path.module}/cloud-init.yaml", {
      tls_key_b64 = base64encode(tls_private_key.consul[count.index].private_key_pem)
      tls_crt_b64 = base64encode(join("\n", [
        trimspace(tls_locally_signed_cert.consul[count.index].cert_pem),
        trimspace(data.terraform_remote_state.ca.outputs.intermediate_ca_certificate_pem),
        trimspace(data.terraform_remote_state.ca.outputs.root_ca_certificate_pem),
      ]))
      ca_pem_b64 = base64encode(data.terraform_remote_state.ca.outputs.root_ca_certificate_pem)
      gossip_b64 = base64encode(jsonencode({ encrypt = random_id.gossip.b64_std }))
      retry_join_b64 = base64encode(jsonencode({
        retry_join = ["provider=gce project_name=${var.project} tag_value=${local.auto_join_tag}"]
      }))
      datacenter_b64 = base64encode(jsonencode({
        datacenter         = var.datacenter
        primary_datacenter = var.primary_datacenter
      }))
      acl_b64 = base64encode(templatefile("${path.module}/templates/acl.hcl", {
        root_token         = random_uuid.root_acl_token.result
        datacenter         = var.datacenter
        primary_datacenter = var.primary_datacenter
        name               = var.cluster_name
      }))
      connect_b64 = base64encode(templatefile("${path.module}/templates/connect.hcl", {
        connect_private_key = var.connect_private_key
        connect_root_cert   = var.connect_root_cert
      }))
      mtls_b64 = base64encode(templatefile("${path.module}/templates/mtls.hcl", {
        verify_incoming_https = false
        verify_incoming_rpc   = true
      }))
    })
  }
}

data "google_compute_image" "configuration_store" {
  family = "configuration-store"
}

locals {
  auto_join_tag = var.cluster_name
}
