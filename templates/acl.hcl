acl {
  enabled        = true
  default_policy = "deny"
  down_policy    = "extend-cache"

  tokens {
    %{ if datacenter == primary_datacenter }
    master = "${root_token}"
    agent  = "${root_token}"
    %{ endif }
  }
}
