connect {
  enabled     = true
  ca_provider = "consul"
  ca_config {
    private_key = "${connect_private_key}"
    root_cert   = "${connect_root_cert}"
  }
}
