resource "tls_private_key" "consul" {
  count = var.cluster_size

  algorithm = "RSA"
  rsa_bits  = 2048
}

resource "tls_cert_request" "consul" {
  count = var.cluster_size

  key_algorithm   = "RSA"
  private_key_pem = tls_private_key.consul[count.index].private_key_pem

  subject {
    common_name  = "Consul Server"
    organization = "Valkyrie Polite Society"
  }

  ip_addresses = [
    "127.0.0.1",
    google_compute_address.public[count.index].address,
  ]

  dns_names = [
    "localhost",
    "consul.service.consul",
    "server.${var.primary_datacenter}.consul",
    trimsuffix(google_dns_record_set.consul.name, "."),
  ]
}

resource "tls_locally_signed_cert" "consul" {
  count = var.cluster_size

  ca_key_algorithm   = "RSA"
  cert_request_pem   = tls_cert_request.consul[count.index].cert_request_pem
  ca_private_key_pem = data.terraform_remote_state.ca.outputs.intermediate_ca_private_key_pem
  ca_cert_pem        = data.terraform_remote_state.ca.outputs.intermediate_ca_certificate_pem

  validity_period_hours = 48000

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
    "client_auth",
  ]
}
