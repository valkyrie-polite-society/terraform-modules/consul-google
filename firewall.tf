resource "google_compute_firewall" "consul" {
  name    = "${var.cluster_name}-http"
  network = data.google_compute_network.default.self_link

  allow {
    protocol = "tcp"
    ports = [
      "8500",
      "8501",
    ]
  }

  target_tags   = [local.auto_join_tag]
  source_ranges = ["0.0.0.0/0"] #tfsec:ignore:GCP003
}
