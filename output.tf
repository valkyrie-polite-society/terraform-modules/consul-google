output "root_token" {
  value     = random_uuid.root_acl_token.result
  sensitive = true
}

output "gossip_key" {
  value     = random_id.gossip.b64_std
  sensitive = true
}
