resource "google_compute_forwarding_rule" "consul" {
  name            = "${var.cluster_name}-https"
  region          = var.region
  ip_address      = google_compute_address.load_balancer.address
  ports           = [8501]
  backend_service = google_compute_region_backend_service.consul.self_link
}

resource "google_compute_region_backend_service" "consul" {
  name                  = var.cluster_name
  region                = var.region
  health_checks         = [google_compute_region_health_check.consul.self_link]
  load_balancing_scheme = "EXTERNAL"

  backend {
    group = google_compute_instance_group.consul.self_link
  }
}
