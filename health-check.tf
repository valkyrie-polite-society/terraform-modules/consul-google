resource "google_compute_region_health_check" "consul" {
  name   = var.cluster_name
  region = var.region

  # The platform does not support mutual TLS for health checks
  http_health_check {
    port         = 8500
    request_path = "/v1/status/leader"
  }
}
