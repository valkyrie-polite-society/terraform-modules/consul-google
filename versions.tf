terraform {
  required_version = ">= 1.0.3"

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 3.78.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 3.1.0"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "3.1.0"
    }
  }
}
