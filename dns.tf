resource "google_dns_record_set" "consul" {
  name         = "${var.cluster_name}.${data.google_dns_managed_zone.domain.dns_name}"
  type         = "A"
  ttl          = 300
  managed_zone = data.google_dns_managed_zone.domain.name
  rrdatas      = [google_compute_address.load_balancer.address]
}

data "google_dns_managed_zone" "domain" {
  name = var.dns_managed_zone
}

resource "google_compute_address" "load_balancer" {
  name   = "${var.cluster_name}-load-balancer"
  region = var.region
}
